#!/usr/bin/bash
# by nu11secur1ty@http://www.nu11secur1ty.com/

cd /opt/
 wget https://download-installer.cdn.mozilla.net/pub/firefox/releases/50.0/linux-x86_64/en-US/firefox-50.0.tar.bz2
    tar xvf firefox-50.0.tar.bz2
    
cd /opt/firefox/ cd $_

#$ which firefox                                            - To see where is your current version.
#/usr/bin/firefox

# mv /usr/bin/firefox /usr/bin/firefox-old                  - Make a backup of your current version.
# ln -s /your-full-path/firefox/firefox /usr/bin/firefox    - Put your path of your current version, to create a startup link.

    mv /bin/firefox /bin/firefox-old
    ln -s /opt/firefox/firefox /bin/
    rm -rf /opt/firefox-50.0.tar.bz2

exit 0;

